/* Aravis - Digital camera library
 *
 * Copyright © 2009-2010 Emmanuel Pacaud
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * Author: Emmanuel Pacaud <emmanuel@gnome.org>
 */

#ifndef ARV_BUFFER_H
#define ARV_BUFFER_H

#if !defined (ARV_H_INSIDE) && !defined (ARAVIS_COMPILATION)
#error "Only <arv.h> can be included directly."
#endif

#include <arvapi.h>

#include <arvtypes.h>
#include <arvbufferstatus.h>

G_BEGIN_DECLS

typedef void (*ArvFrameCallback)	(ArvBuffer *buffer);

#define ARV_TYPE_BUFFER             (arv_buffer_get_type ())
#define ARV_BUFFER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), ARV_TYPE_BUFFER, ArvBuffer))
#define ARV_BUFFER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), ARV_TYPE_BUFFER, ArvBufferClass))
#define ARV_IS_BUFFER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ARV_TYPE_BUFFER))
#define ARV_IS_BUFFER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), ARV_TYPE_BUFFER))
#define ARV_BUFFER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), ARV_TYPE_BUFFER, ArvBufferClass))

typedef struct _ArvBufferPrivate ArvBufferPrivate;
typedef struct _ArvBufferClass ArvBufferClass;

struct _ArvBuffer {
	GObject	object;

	ArvBufferPrivate *priv;
};

struct _ArvBufferClass {
	GObjectClass parent_class;
};

GType GENICAM_API arv_buffer_get_type (void);

ArvBuffer *		GENICAM_API arv_buffer_new_allocate		(size_t size);
ArvBuffer *		GENICAM_API arv_buffer_new 			(size_t size, void *preallocated);
ArvBuffer * 		GENICAM_API arv_buffer_new_full		(size_t size, void *preallocated,
						 	void *user_data, GDestroyNotify user_data_destroy_func);

ArvBufferStatus		GENICAM_API arv_buffer_get_status		(ArvBuffer *buffer);

const void *		GENICAM_API arv_buffer_get_user_data	(ArvBuffer *buffer);

ArvBufferPayloadType	GENICAM_API arv_buffer_get_payload_type	(ArvBuffer *buffer);
guint64			GENICAM_API arv_buffer_get_timestamp	(ArvBuffer *buffer);
void			GENICAM_API arv_buffer_set_timestamp	(ArvBuffer *buffer, guint64 timestamp_ns);
guint64			GENICAM_API arv_buffer_get_system_timestamp	(ArvBuffer *buffer);
void			GENICAM_API arv_buffer_set_system_timestamp	(ArvBuffer *buffer, guint64 timestamp_ns);
guint32 		GENICAM_API arv_buffer_get_frame_id 	(ArvBuffer *buffer);
const void *		GENICAM_API arv_buffer_get_data		(ArvBuffer *buffer, size_t *size);

void			GENICAM_API arv_buffer_get_image_region		(ArvBuffer *buffer, gint *x, gint *y, gint *width, gint *height);
gint			GENICAM_API arv_buffer_get_image_width		(ArvBuffer *buffer);
gint			GENICAM_API arv_buffer_get_image_height		(ArvBuffer *buffer);
gint			GENICAM_API arv_buffer_get_image_x			(ArvBuffer *buffer);
gint			GENICAM_API arv_buffer_get_image_y			(ArvBuffer *buffer);
ArvPixelFormat		GENICAM_API arv_buffer_get_image_pixel_format	(ArvBuffer *buffer);

const void *		GENICAM_API arv_buffer_get_chunk_data	(ArvBuffer *buffer, guint64 chunk_id, size_t *size);

G_END_DECLS

#endif
