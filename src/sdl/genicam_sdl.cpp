// (c) 2019 Glazomer Ltd.
//
// This example deploys ARVAPI to render grayscale GENICAM camera output to the SDL surface.
// ARVAPI keeps comprehensive GLib-based logic inside libgenicam shared library and offers
// much simpler and robust user interface.

#include <atomic>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <mutex>
#include <sstream>
#include <vector>
#include <SDL.h>

#include "genicam.h"
#include "arvbufferstatus.h"

using namespace std;

const double displayRefreshRate = 1.0 / 30; // 30 FPS
const double fpsRefreshRate = 1.0; // once per second
unsigned int nframes = 0;

mutex mtx;

atomic<bool> isInterrupted;
SDL_Window* window;
SDL_Renderer* renderer;

class GenicamSDL
{
protected :

	SDL_Renderer* renderer;
	SDL_Texture* texture;
	vector<char> yuvPixels;

	virtual void updateDisplay(char* pixels)
	{
		if (SDL_UpdateTexture(texture, NULL, pixels, getPitch()) != 0)
		{
			fprintf(stderr, "SDL_UpdateTexture Error: %s\n", SDL_GetError());
			exit(EXIT_FAILURE);
		}
		SDL_RenderCopy(renderer, texture, nullptr, nullptr);
		SDL_RenderPresent(renderer);
	}

public :

	const int width, height;

	GenicamSDL(SDL_Renderer* renderer_, int width_, int height_) : renderer(renderer_), width(width_), height(height_)
	{
		texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_IYUV,
			SDL_TEXTUREACCESS_STREAMING, width, height);
		if (texture == nullptr)
		{
			fprintf(stderr, "SDL_CreateTextureFromSurface Error: %s\n", SDL_GetError());
			exit(EXIT_FAILURE);
		}

		yuvPixels.resize(width * height / 2 * 3);

		// Fill U and V planes with 128 value - "gray color".
		// The Y plane will be overwritten by camera.
		memset(reinterpret_cast<char*>(&yuvPixels[width * height]),
			128, yuvPixels.size() - width * height);
	}
	
	virtual int getPitch()
	{
		return width;
	}

	void updateFrame(const void* data)
	{
		// Copy grayscale pixels into YUV luma pixels,
		// which effectively means a grayscale YUV image, which
		// is way simpler than to deal with RGB.
		memcpy(reinterpret_cast<char*>(&yuvPixels[0]), data, width * height);
	}

	virtual void updateDisplay()
	{
		updateDisplay(reinterpret_cast<char*>(&yuvPixels[0]));
	}

	virtual ~GenicamSDL()
	{
		SDL_DestroyTexture(texture);
	}
};

class RotatableGenicamSDL : GenicamSDL
{
	// Rotation degrees.
	int degrees;

	vector<char> yuvPixelsBuffer;

	// We need to lock texture update, while the texture
	// is being re-created for rotation.
	mutex mtx;

public :

	RotatableGenicamSDL(SDL_Renderer* renderer, int width, int height) : GenicamSDL(renderer, width, height), degrees(0)
	{
		yuvPixelsBuffer.resize(width * height / 2 * 3);

		// Fill U and V planes with 128 value - "gray color".
		// The Y plane will be overwritten by camera.
		memset(reinterpret_cast<char*>(&yuvPixelsBuffer[width * height]),
			128, yuvPixels.size() - width * height);
	}

	virtual void updateDisplay()
	{
		memcpy(&yuvPixelsBuffer[0], &yuvPixels[0], width * height);
	
		// Lock the texture from re-creation.
		lock_guard<mutex> lck(mtx);
	
		// Apply rotation to YUV pixels.
		switch (degrees)
		{
		case 0 :
			break;
		case 180 :
			for (int j = 0; j < height / 2; j++)
				for (int i = 0; i < width; i++)
				{
					char swap = yuvPixelsBuffer[j * width + i];
					yuvPixelsBuffer[j * width + i] = yuvPixelsBuffer[(height - j - 1) * width + i];
					yuvPixelsBuffer[(height - j - 1) * width + i] = swap;
				}
			break;
		case 90 :
			{
				vector<char> yuvPixelsCopy(width * height);
				for (int j = 0; j < height; j++)
					for (int i = 0; i < width; i++)
						yuvPixelsCopy[i * height + j] = yuvPixelsBuffer[j * width + i];
				memcpy(&yuvPixelsBuffer[0], &yuvPixelsCopy[0], width * height);
			}
			break;
		case 270 :
			{
				vector<char> yuvPixelsCopy(width * height);
				for (int j = 0; j < height; j++)
					for (int i = 0; i < width; i++)
						yuvPixelsCopy[(width - 1 - i) * height + height - j - 1] = yuvPixelsBuffer[j * width + i];
				memcpy(&yuvPixelsBuffer[0], &yuvPixelsCopy[0], width * height);
			}
			break;
		}

		GenicamSDL::updateDisplay(reinterpret_cast<char*>(&yuvPixelsBuffer[0]));
	}

	virtual int getPitch()
	{
		if ((degrees == 0) || (degrees == 180)) return width;
		
		return height;
	}

	void rotate90()
	{
		// Lock the display from updating.
		lock_guard<mutex> lck(mtx);

		degrees += 90;
		degrees %= 360;

#ifndef _WIN32
		printf("Rotated to %d degrees\n", degrees);
#endif

		// Re-create rotated textures.
		SDL_DestroyTexture(texture);

		unsigned int textureWidth = width;
		unsigned int textureHeight = height;
		if ((degrees == 90) || (degrees == 270))
		{
			textureWidth = height;
			textureHeight = width;
		}

		texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_IYUV,
			SDL_TEXTUREACCESS_STREAMING, textureWidth, textureHeight);
		if (texture == nullptr)
		{
			fprintf(stderr, "SDL_CreateTextureFromSurface Error: %s\n", SDL_GetError());
			exit(EXIT_FAILURE);
		}
	}
};

static void describeArvBufferStatus(int status)
{
	switch (status)
	{
	case ARV_BUFFER_STATUS_UNKNOWN:
		fprintf(stderr, "ARV_BUFFER_STATUS_UNKNOWN: unknown status\n");
		break;
	case ARV_BUFFER_STATUS_SUCCESS:
		fprintf(stderr, "ARV_BUFFER_STATUS_SUCCESS: the buffer contains a valid image\n");
		break;
	case ARV_BUFFER_STATUS_CLEARED:
		fprintf(stderr, "ARV_BUFFER_STATUS_CLEARED: the buffer is cleared\n");
		break;
	case ARV_BUFFER_STATUS_TIMEOUT:
		fprintf(stderr, "ARV_BUFFER_STATUS_TIMEOUT: timeout was reached before all packets are received\n");
		break;
	case ARV_BUFFER_STATUS_MISSING_PACKETS:
		fprintf(stderr, "ARV_BUFFER_STATUS_MISSING_PACKETS: stream has missing packets\n");
		break;
	case ARV_BUFFER_STATUS_WRONG_PACKET_ID:
		fprintf(stderr, "ARV_BUFFER_STATUS_WRONG_PACKET_ID: stream has packet with wrong id\n");
		break;
	case ARV_BUFFER_STATUS_SIZE_MISMATCH:
		fprintf(stderr, "ARV_BUFFER_STATUS_SIZE_MISMATCH: the received image didn't fit in the buffer data space\n");
		break;
	case ARV_BUFFER_STATUS_FILLING:
		fprintf(stderr, "ARV_BUFFER_STATUS_FILLING: the image is currently being filled\n");
		break;
	case ARV_BUFFER_STATUS_ABORTED:
		fprintf(stderr, "ARV_BUFFER_STATUS_ABORTED: the filling was aborted before completion\n");
		break;
	}
}

// Called when Aravis backend captures a new frame
// from GenICam camera.
static void onCaptureFrame(void* buffer, void* user)
{
	unique_lock<mutex> lck(mtx, try_to_lock);
	if(!lck.owns_lock()) return;

	GenicamSDL* genicamSDL = reinterpret_cast<GenicamSDL*>(user);

	if (!buffer)
	{
		fprintf(stderr, "Invalid frame buffer pointer: %p\n", buffer);
		return;
	}

	// Check the frame buffer is valid.
	int status = genicam_get_status(buffer);
	if (status != ARV_BUFFER_STATUS_SUCCESS)
	{
		fprintf(stderr, "Unexpected error status ");
		describeArvBufferStatus(status);
		return;
	}
	
	// Check the frame buffer contains what we expect.
	size_t szbuffer = 0;
	if (!genicam_get_data(buffer, &szbuffer))
	{ 
		fprintf(stderr, "Cannot get the frame buffer data\n");
		return;
	}
	if (szbuffer != genicamSDL->height * genicamSDL->width)
	{
		fprintf(stderr, "Invalid frame buffer size: %zu != %d (expected)\n",
			szbuffer, genicamSDL->height * genicamSDL->width);
		return;
	}
	
	genicamSDL->updateFrame(genicam_get_data(buffer, NULL));
	nframes++;
}

// Called when Aravis backend loses connection to
// the GenICam camera for some reason.
static void onCameraDisconnected(void* user)
{
	GenicamSDL* genicamSDL = reinterpret_cast<GenicamSDL*>(user);
	fprintf(stderr, "The camera has been disconnected unexpectedly\n");
	isInterrupted = true;
}

int main(int argc, char* argv[])
{
	isInterrupted = false;
	SDL_Window* window;
	SDL_Renderer* renderer;

	// Try to pick up the first available Genicam camera.
	void* camera = genicam_new();
	if (!camera)
	{
		fprintf(stderr, "No camera found\n");
		return EXIT_FAILURE;
	}

	int x, y, width, height;
	genicam_get_region(camera, &x, &y, &width, &height);
	const char* vendor = genicam_get_vendor_name(camera);
	const char* model = genicam_get_model_name(camera);
	const double fps = genicam_get_frame_rate(camera);

	printf("Using camera %s %s (%d x %d, %dfps max)\n", vendor, model, width, height, (int)fps);

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		fprintf(stderr, "SDL_Init Error: %s\n", SDL_GetError());
		return EXIT_FAILURE;
	}

	window = SDL_CreateWindow("Genicam SDL viewer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if (window == nullptr)
	{
		fprintf(stderr, "SDL_CreateWindow Error: %s\n", SDL_GetError());
		return EXIT_FAILURE;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == nullptr)
	{
		fprintf(stderr, "SDL_CreateRenderer Error: %s\n", SDL_GetError());
		return EXIT_FAILURE;
	}

	{
		RotatableGenicamSDL genicamSDL(renderer, width, height);

		const int frameBufferCount = 5;
		vector<char> frameBuffers(frameBufferCount * width * height);
		if (genicam_start(camera, frameBufferCount, reinterpret_cast<char*>(&frameBuffers[0]),
			onCaptureFrame, onCameraDisconnected, &genicamSDL))
		{
			fprintf(stderr, "Failed to start GENICAM camera\n");
			return EXIT_FAILURE;
		}

		Uint64 displayTimestamp;
		Uint64 fpsTimestamp;

		displayTimestamp = SDL_GetPerformanceCounter();
		fpsTimestamp = SDL_GetPerformanceCounter();
		
		genicam_loop_create(camera);

		// TODO Make checks here less frequent.
		while (!isInterrupted)
		{
			genicam_loop_iterate(camera);

			SDL_Event event;
			while (SDL_PollEvent(&event))
			{
				if (event.type == SDL_QUIT)
				{
					isInterrupted = true;
					break;
				}
				if (event.type == SDL_KEYDOWN)
				{
					if (event.key.keysym.sym == SDLK_ESCAPE)
					{
						isInterrupted = true;
						break;
					}
					if (event.key.keysym.sym == SDLK_r)
					{
						genicamSDL.rotate90();
						
						// Swap window dimension.
						int widthOld = width;
						int heightOld = height;
						width = heightOld;
						height = widthOld;						
						SDL_SetWindowSize(window, width, height);
					}
				}
			}

			// Check if it's time to render frame.
			Uint64 displayTimestampNew = SDL_GetPerformanceCounter();
			const static Uint64 freq = SDL_GetPerformanceFrequency();
			double displayElapsedSeconds = (displayTimestampNew - displayTimestamp) / static_cast<double>(freq);

			if (displayElapsedSeconds >= displayRefreshRate)
			{
				displayTimestamp = displayTimestampNew;
				
				genicamSDL.updateDisplay();
			}

			// Check if it's time to report FPS.
			Uint64 fpsTimestampNew = SDL_GetPerformanceCounter();
			double fpsElapsedSeconds = (fpsTimestampNew - fpsTimestamp) / static_cast<double>(freq);

			if (fpsElapsedSeconds >= fpsRefreshRate)
			{
				fpsTimestamp = fpsTimestampNew;
				double fps = nframes / fpsElapsedSeconds;
				stringstream ss;
				ss << "Genicam SDL viewer (";
				ss << (int)fps << "fps)";
				const string& title = ss.str();
				SDL_SetWindowTitle(window, title.c_str());
				nframes = 0;
#ifndef _WIN32
				printf("%d Hz\n", (int)fps);
#endif
			}
		}

		// Do not destroy while frame capture callback is still active.
		mtx.lock();

		genicam_loop_quit(camera);
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	genicam_stop(camera);
	genicam_release(camera);
	
	return EXIT_SUCCESS;
}

