#ifndef ARV_REALTIME_PRIVATE_H
#define ARV_REALTIME_PRIVATE_H

#if !defined (ARV_H_INSIDE) && !defined (ARAVIS_COMPILATION)
#error "Only <arv.h> can be included directly."
#endif

#include <arvapi.h>

#include <arvrealtime.h>
#include <gio/gio.h>
#include <sys/types.h>

int 		GENICAM_API arv_rtkit_get_max_realtime_priority	(GDBusConnection *connection, GError **error);
int 		GENICAM_API arv_rtkit_get_min_nice_level 		(GDBusConnection *connection, GError **error);
gint64		GENICAM_API arv_rtkit_get_rttime_usec_max 		(GDBusConnection *connection, GError **error);
void		GENICAM_API arv_rtkit_make_realtime 		(GDBusConnection *connection, pid_t thread, int priority, GError **error);
void		GENICAM_API arv_rtkit_make_high_priority 		(GDBusConnection *connection, pid_t thread, int nice_level, GError **error);

#endif
