# GENICAM: HQ Machine Vision runtime based on Aravis

This is a simplified cross-platform runtime for GENICAM vision cameras (Contrastech, HikVision, etc.) based on Aravis project by Emmanuel Pacaud of the GNOME Foundation. The purpose of this open-source runtime is to replace the closed-source blobs shipped by camera vendors.

![alt text](screenshots/genicam_sdl.png)

## Building on Ubuntu Linux

Along with compilers and build tools, the following dependencies must be installed:

```
sudo apt install libglib2.0-dev libusb-1.0-0-dev libxml2-dev
```

Build with CMake:

```
mkdir build
cd build
cmake ..
make -j12
./genicam_test
```

Build for debugging with libusb verbose logging:

```
mkdir build_debug
cd build_debug
cmake -DWITH_DEBUG_LOG=ON -DCMAKE_BUILD_TYPE=Debug ..
make -j12
./genicam_test
```

## Building on Windows with MSYS2

Along with compilers and build tools, the following dependencies must be installed:

```
pacman -S mingw64/mingw-w64-x86_64-glib2 mingw64/mingw-w64-x86_64-libusb mingw64/mingw-w64-x86_64-libusb-compat-git mingw64/mingw-w64-x86_64-libxml2 mingw64/mingw-w64-x86_64-SDL2
```

Build with CMake using UNIX Makefiles:

```
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make -j12
./genicam_test.exe
```

## Building on Apple

Use Homebrew to install compiler toolchain and the following packages:

```
brew install pkg-config libusb glib
```

## Emulating a GigE camera

If no physical GenICam is available, device emulator can be launched:

```
./genicam_emulator -i wlp8s0 -g ./genicam_emulator.xml
```

A separate launch of `genicam_test` or `genicam_sdl` will then discover and connect to the emulated camera.

## Sample output

```
Looking for the first available camera
vendor name           = U3V
model name            = MV-CA003-21UM
device id             = (null)
image width           = 640
image height          = 480
horizontal binning    = 1
vertical binning      = 1
payload               = 307200 bytes
exposure              = 1068 µs
gain                  = 0 dB
uv bandwidth limit     = -1 [92168400..387500000]
Frame rate = 1076 Hz
Frame rate = 814 Hz
Frame rate = 813 Hz
Frame rate = 812 Hz
Completed buffers = 3515
Failures          = 0
Underruns         = 0
```

## Troubleshooting

In case GenICam device is not found, try to add debugging flag:

```
./genicam_test.exe -d interface
Looking for the first available camera
Failed to open USB device, error = LIBUSB_ERROR_NOT_FOUND
Found 0 USB3Vision device (among 4 USB devices)
No camera found
```

If the camera cable is connected, but the error above is shown on **Windows**, then a libusb-compatible driver is missing, e.g. WinUSB - you can install it with [Zadig](https://zadig.akeo.ie):

![alt text](screenshots/zadig.png)

```
./genicam_test.exe -d all
Looking for the first available camera
Found 1 USB3Vision device (among 4 USB devices)
[UvDevice::new] Vendor  = U3V
[UvDevice::new] Product = MV-CA003-21UM
[UvDevice::new] S/N     = 00D09251033
[UvDevice::new] Using control endpoint 1, interface 0
[UvDevice::new] Using data endpoint 3, interface 2
[UvDevice::new] Failed to claim USB interface to 'MV-CA003-21UM - #00D09251033'
No camera found
```

In this case on Windows, make sure the WinUSB driver is installed in Zadig not only for the "Interface 0", but for the entire "Composite parent" device (Menu -> Options -> Ignore Hubs or Composite Parents - uncheck).

The camera might be inaccessible:

```
./genicam_test -d interface
Looking for the first available camera
Failed to open USB device, error = LIBUSB_ERROR_ACCESS
Found 0 USB3Vision device (among 9 USB devices)
```

If this is a case on **Linux**, make sure the application has permissions to access the device file (re-running with `sudo` is the easiest way to check). Another solution is to add a permissive udev rule:

```
$ cat /etc/udev/rules.d/80-genicam.rules 
#HIKVISION U3V MV-CA003-21UM
ATTRS{idVendor}=="2bdf",ATTRS{idProduct}=="0001",MODE="0666"
```

If a GenICam application has crashed, the camera may still be kept busy and unavailable for subsequent connections. In such case, use a `genicam_reset` utility to restart the camera, providing the IDs, for example:

```
./genicam_reset 2bdf:0001
```

In rare cases, a proper camera restart may require to physically re-plug the USB cable.

