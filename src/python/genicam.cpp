#include "genicam.h"

#include <pybind11/pybind11.h>
#include <pybind11/functional.h> // TODO needed or not?
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>
#include <pybind11/numpy.h>
#include <memory>
#include <string>
#include <vector>

namespace py = pybind11;

using namespace std;

namespace {

class Camera;

class FrameBuffer
{
	const Camera& parent;
	void* buffer;

	int width, height;

	FrameBuffer(const Camera& parent_, void* buffer_);
	
	friend class Camera;

public :

	// Check the frame buffer is valid.
	bool isValid();

	py::array_t<uint8_t> getGrayscale();
	py::array_t<uint8_t> getRGB();
};

class Camera
{
	void* handle;

	Camera(void* handle_) : handle(handle_), started(false) { }

	// Not publicly constructible, copyable, or movable.
	Camera(Camera &&) = delete;
	Camera(const Camera &) = delete;
	Camera &operator=(Camera &&) = delete;
	Camera &operator=(const Camera &) = delete;

	// Check the frame buffer is valid.
	bool isValid(void* buffer) const
	{
		if (!handle) return false;
		
		return genicam_get_status(buffer) != 0;
	}

	void getGrayscale(void* buffer, uint8_t** result) const
	{
		if (!result) return;
		*result = NULL;

		if (!handle) return;

		size_t szbuffer = 0;
		const char* data = genicam_get_data(buffer, &szbuffer);
		if (!data) return;

		*result = new uint8_t[szbuffer];
		memcpy(*result, data, szbuffer);
	}

	void getRGB(void* buffer, uint8_t** result) const
	{
		if (!result) return;
		*result = NULL;

		if (!handle) return;

		size_t szbuffer = 0;
		const char* data = genicam_get_data(buffer, &szbuffer);
		if (!data) return;

		*result = new uint8_t[szbuffer * 3];
		uint8_t* result_ = *result;
		for (int i = 0; i < szbuffer; i++)
		{
			result_[3 * i] = data[i];
			result_[3 * i + 1] = data[i];
			result_[3 * i + 2] = data[i];
		}
	}

	friend class FrameBuffer;

public :

	~Camera()
	{
		if (!handle) return;
		genicam_release(handle);
	}

	static py::object connect()
	{
		// Try to pick up the first available Camera camera.
		void* handle = genicam_new();
		if (!handle) return py::none();

		return py::cast(new Camera(handle));
	}

	vector<int> getRegion() const
	{
		if (!handle) return vector<int>();
		
		vector<int> result(4);
		genicam_get_region(handle,
			reinterpret_cast<int*>(&result[0]), reinterpret_cast<int*>(&result[1]),
			reinterpret_cast<int*>(&result[2]), reinterpret_cast<int*>(&result[3]));
		return result;
	}

	string getVendorName()
	{
		if (!handle) return "";
		
		const char* result = genicam_get_vendor_name(handle);
		if (!result) return "";
		return (string)result;
	}
	
	string getModelName()
	{
		if (!handle) return "";

		const char* result = genicam_get_model_name(handle);
		if (!result) return "";
		return (string)result;
	}
	
	string getDeviceID()
	{
		if (!handle) return "";
		
		const char* result = genicam_get_device_id(handle);
		if (!result) return "";
		return (string)result;
	}
	
	double getFrameRate()
	{
		if (!handle) return 0.0;
		
		return genicam_get_frame_rate(handle);
	}

	int getPayload()
	{
		if (!handle) return 0;
		
		return genicam_get_payload(handle);
	}

private :

	static void onCaptureFrameFuncWrapper(void* buffer, void* user)
	{
		if (!buffer) return;
		if (!user) return;

		Camera* genicam = reinterpret_cast<Camera*>(user);
		
		genicam->onCaptureFrameFunc(FrameBuffer(*genicam, buffer), genicam->user);
	}

	static void onCameraDisconnectedFuncWrapper(void* user)
	{
		if (!user) return;

		Camera* genicam = reinterpret_cast<Camera*>(user);
		
		genicam->onCameraDisconnectedFunc(genicam->user);
	}

public :

	typedef std::function<void(FrameBuffer buffer, py::object user)> OnCaptureFrame;
	typedef std::function<void(py::object user)> OnCameraDisconnected;

	bool started;
	OnCaptureFrame onCaptureFrameFunc;
	OnCameraDisconnected onCameraDisconnectedFunc;
	py::object user;
	
	size_t bufferFrameCount;
	vector<uint8_t> bufferFrames;

	bool start(int bufferFrameCount_, OnCaptureFrame onCaptureFrameFunc_,
		OnCameraDisconnected onCameraDisconnectedFunc_, py::object user_)
	{
		if (!handle) return false;
	
		// Check if already started.
		if (started) return false;
		
		// Update user callbacks.
		onCaptureFrameFunc = onCaptureFrameFunc_;
		onCameraDisconnectedFunc = onCameraDisconnectedFunc_;
		user = user_;

		// Renew self-managed frames buffer.
		bufferFrameCount = bufferFrameCount_;
		int x = 0, y = 0, width = 0, height = 0;
		genicam_get_region(handle, &x, &y, &width, &height);
		bufferFrames.resize(bufferFrameCount * width * height);

		started = genicam_start(handle, bufferFrameCount,
			reinterpret_cast<char*>(&bufferFrames[0]),
			onCaptureFrameFuncWrapper, onCameraDisconnectedFuncWrapper, this) != 0;

		return started;
	}

	void loopCreate()
	{
		if (!handle) return;
		
		genicam_loop_create(handle);
	}

	void loopIterate()
	{
		if (!handle) return;

		genicam_loop_iterate(handle);
	}

	void loopQuit()
	{
		if (!handle) return;
		
		genicam_loop_quit(handle);
	}

	void stop()
	{
		if (!handle) return;
		
		genicam_stop(handle);
		
		started = false;
	}
};

FrameBuffer::FrameBuffer(const Camera& parent_, void* buffer_) : parent(parent_), buffer(buffer_)
{
	vector<int> region = parent.getRegion();
	width = region[2] - region[0];
	height = region[3] - region[1];
}

// Check the frame buffer is valid.
bool FrameBuffer::isValid()
{
	if (!buffer) return false;
	
	return parent.isValid(buffer) != 0;
}

py::array_t<uint8_t> FrameBuffer::getGrayscale()
{
	if (!buffer) return py::none();

	uint8_t* result = NULL;
	parent.getGrayscale(buffer, &result);
	if (!result) return py::none();

	// Create a Python object that will free the allocated
	// memory when destroyed:
	py::capsule free_when_done(result, [](void* ptr)
	{
		if (!ptr) return;
		uint8_t* result = reinterpret_cast<uint8_t*>(ptr);
		delete[] result;
	});

	return py::array_t<uint8_t>(
            { height, width }, // shape
            { width * sizeof(uint8_t), sizeof(uint8_t) }, // C-style contiguous strides for double
            result, // the data pointer
            free_when_done); // numpy array references this parent
}

py::array_t<uint8_t> FrameBuffer::getRGB()
{
	if (!buffer) return py::none();

	uint8_t* result = NULL;
	parent.getRGB(buffer, &result);
	if (!result) return py::none();

	// Create a Python object that will free the allocated
	// memory when destroyed:
	py::capsule free_when_done(result, [](void* ptr)
	{
		if (!ptr) return;
		uint8_t* result = reinterpret_cast<uint8_t*>(ptr);
		delete[] result;
	});

	return py::array_t<uint8_t>(
		{ height, width, 3}, // shape
		{ width * 3 * sizeof(uint8_t), 3 * sizeof(uint8_t), sizeof(uint8_t) }, // C-style contiguous strides for double
		result, // the data pointer
		free_when_done); // numpy array references this parent
}

} // namespace

extern "C" GENICAM_API void genicam_init_python(void* pyglazomer_, const char* apikey)
{
	py::module& pyglazomer = *reinterpret_cast<py::module*>(pyglazomer_);
	py::module genicam = pyglazomer.def_submodule("genicam");

	genicam.doc() = "Python interface for Genicam camera frame capture";

	py::class_<FrameBuffer>(genicam, "FrameBuffer")
		.def("isValid", &FrameBuffer::isValid, "Check the frame buffer is valid")
		.def("getGrayscale", &FrameBuffer::getGrayscale)
		.def("getRGB", &FrameBuffer::getRGB);

	py::class_<Camera>(genicam, "Camera")
		.def("connect", &Camera::connect, "Connect to the first available Camera camera")
		.def("getRegion", &Camera::getRegion)
		.def("getVendorName", &Camera::getVendorName) 
		.def("getModelName", &Camera::getModelName)
		.def("getDeviceID", &Camera::getDeviceID)
		.def("getFrameRate", &Camera::getFrameRate)
		.def("getPayload", &Camera::getPayload)
		.def("start", &Camera::start)
		.def("loopCreate", &Camera::loopCreate)
		.def("loopIterate", &Camera::loopIterate)
		.def("loopQuit", &Camera::loopQuit)
		.def("stop", &Camera::stop);
}

