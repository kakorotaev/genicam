#ifndef ARV_API_H
#define ARV_API_H

#ifdef _WIN32
#ifdef genicam_EXPORTS
#define GENICAM_API __declspec(dllexport)
#else
#ifndef GENICAM_STATIC
#define GENICAM_API __declspec(dllimport)
#else
#define GENICAM_API
#endif
#endif
#else // _WIN32
#define GENICAM_API
#endif // _WIN32

#endif // ARV_API_H

