#ifndef GENICAM_H
#define GENICAM_H

#include <arvapi.h>
#include <stddef.h> // size_t

// In this header we provide wrappers for standard Aravis API,
// in order to hide possibly unwanted dependency on GLib from
// the user side.

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*on_capture_frame_t)(void* buffer, void* user);
typedef void (*on_camera_disconnected_t)(void* user);

// Try to pick up the first available Genicam camera.
GENICAM_API void* genicam_new();

GENICAM_API void genicam_get_region(void* camera, int* x, int* y, int* width, int* height);

GENICAM_API const char* genicam_get_vendor_name(void* camera);

GENICAM_API const char* genicam_get_model_name(void* camera);

GENICAM_API const char* genicam_get_device_id(void* camera);

GENICAM_API double genicam_get_frame_rate(void* camera);

GENICAM_API void genicam_release(void* camera);

// Check the frame buffer is valid.
GENICAM_API int genicam_get_status(void* buffer);

GENICAM_API const char* genicam_get_data(void* buffer, size_t* szbuffer);

GENICAM_API int genicam_get_payload(void* camera);

GENICAM_API int genicam_start(void* camera, int nframes, char* frame_buffer,
	on_capture_frame_t on_capture_frame,
	on_camera_disconnected_t on_camera_disconnected, void* user);

GENICAM_API void genicam_loop_create(void* camera);

GENICAM_API void genicam_loop_iterate(void* camera);

GENICAM_API void genicam_loop_quit(void* camera);

GENICAM_API void genicam_stop(void* camera);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // GENICAM_H

