#include <glib.h>
#include <glib-object.h>
#include <arv.h>
#include <stdlib.h>
#if !defined(__APPLE__)
#include <malloc.h>
#endif

#include "genicam.h"

typedef void (*on_capture_frame_t)(void* buffer, void* user);
typedef void (*on_camera_disconnected_t)(void* user);

typedef struct
{
	ArvCamera* camera;
	ArvStream* stream;

	GMainLoop* loop;
	GMainContext* context;

	on_capture_frame_t on_capture_frame;
	on_camera_disconnected_t on_camera_disconnected;

	void* user;
}
genicam_t;

// Initialization chain to be forcibly deinitialized by atexit()
typedef struct camerachain_t
{
	genicam_t* handle;
	struct camerachain_t* next;
}
camerachain_t;

static camerachain_t camerachain = { .handle = NULL, .next = NULL };
static camerachain_t* camerachain_next = &camerachain;

// Release all allocated cameras unconditionally.
static void genicam_release_all(void)
{
	camerachain_t* ptr = &camerachain;
	while (1)
	{
		if (!ptr) break;

		genicam_release(ptr->handle);
		camerachain_t* next = ptr->next;
		ptr->next = NULL;
		if (ptr != &camerachain) free(ptr);
		ptr = next;
	}
}

// Try to pick up the first available Genicam camera.
void* genicam_new()
{
	static int atexit_set = 0;
	if (!atexit_set)
	{
		// Adding atexit() handler to unconditionally
		// release all cameras when application exits.
		atexit(&genicam_release_all);
		atexit_set = 1;
	}

	ArvCamera* camera = arv_camera_new(NULL);
	if (!camera) return NULL;

	genicam_t* handle = malloc(sizeof(genicam_t));
	if (!handle) return NULL;
	memset(handle, 0, sizeof(genicam_t));
	handle->camera = camera;

	// Memorize camera in deinitialization chain.
	camerachain_next->next = malloc(sizeof(camerachain_t));
	memset(camerachain_next->next, 0, sizeof(camerachain_t));
	if (!camerachain_next->next) return NULL;
	camerachain_next->handle = handle;
	camerachain_next = camerachain_next->next;

	return handle;
}

void genicam_get_region(void* camera, int* x_, int* y_, int* width_, int* height_)
{
	genicam_t* handle = (genicam_t*)camera;
	if (!handle) return;
	if (!handle->camera) return;
	
	int x, y, width, height;
	arv_camera_get_region(handle->camera, &x, &y, &width, &height);
	if (x_) *x_ = x;
	if (y_) *y_ = y;
	if (width_) *width_ = width;
	if (height_) *height_ = height;
}

const char* genicam_get_vendor_name(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	if (!handle) return NULL;
	if (!handle->camera) return NULL;
	return arv_camera_get_vendor_name(handle->camera);
}

const char* genicam_get_model_name(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	if (!handle) return NULL;
	if (!handle->camera) return NULL;
	return arv_camera_get_model_name(handle->camera);
}

const char* genicam_get_device_id(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	if (!handle) return NULL;
	if (!handle->camera) return NULL;
	return arv_camera_get_device_id(handle->camera);
}

double genicam_get_frame_rate(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	if (!handle) return 0;
	if (!handle->camera) return 0;
	return arv_camera_get_frame_rate(handle->camera);
}

void genicam_release(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	if (!handle) return;
	if (handle->loop) genicam_loop_quit(camera);
	if (handle->stream) genicam_stop(camera);
	if (handle->camera)
	{
		g_object_unref(handle->camera);
		handle->camera = NULL;
	}

	// Do not free() handle, as it will be freed by atexit() handler.
}

// Check the frame buffer is valid.
int genicam_get_status(void* buffer)
{
	return arv_buffer_get_status(buffer);
}

const char* genicam_get_data(void* buffer, size_t* szbuffer)
{
	return arv_buffer_get_data(buffer, szbuffer);
}

int genicam_get_payload(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	if (!handle) return 0;
	if (!handle->camera) return 0;
	return arv_camera_get_payload(handle->camera);
}

// Called when Aravis backend captures a new frame
// from GenICam camera.
static void on_capture_frame_impl(ArvStream* stream, void* camera)
{
	if (!stream || !camera) return;

	ArvBuffer* buffer = arv_stream_try_pop_buffer(stream);

	// Call user function
	genicam_t* handle = (genicam_t*)camera;
	handle->on_capture_frame(buffer, handle->user);	

	arv_stream_push_buffer(stream, buffer);
}

// Called when Aravis backend loses connection to
// the GenICam camera for some reason.
static void on_camera_disconnected_impl(ArvStream* stream, void* camera)
{
	if (!stream || !camera) return;

	// Call user function
	genicam_t* handle = (genicam_t*)camera;
	handle->on_camera_disconnected(handle->user);	
}

int genicam_start(void* camera, int nframes, char* frame_buffer,
	on_capture_frame_t on_capture_frame,
	on_camera_disconnected_t on_camera_disconnected, void* user)
{
	genicam_t* handle = (genicam_t*)camera;
	if (!handle) return -1;

	if (!handle->camera) return -1;

	ArvStream* stream = arv_camera_create_stream(handle->camera, NULL, NULL);
	if (!stream)
	{
		genicam_release(camera);
		return -1;
	}

	arv_camera_set_chunks(handle->camera, NULL);

	if (ARV_IS_GV_STREAM(stream))
	{
		const gboolean arv_option_auto_socket_buffer = FALSE;
		if (arv_option_auto_socket_buffer)
			g_object_set(stream,
				"socket-buffer", ARV_GV_STREAM_SOCKET_BUFFER_AUTO,
				"socket-buffer-size", 0,
				NULL);

		const gboolean arv_option_no_packet_resend = FALSE;
		if (arv_option_no_packet_resend)
			g_object_set(stream,
				"packet-resend", ARV_GV_STREAM_PACKET_RESEND_NEVER,
				NULL);

		const double arv_option_packet_request_ratio = -1.0;
		if (arv_option_packet_request_ratio >= 0.0)
			g_object_set(stream,
				"packet-request-ratio", arv_option_packet_request_ratio,
				NULL);

		const unsigned int arv_option_packet_timeout = 20;
		const unsigned int arv_option_frame_retention = 100;
		g_object_set(stream,
			"packet-timeout", (unsigned) arv_option_packet_timeout * 1000,
			"frame-retention", (unsigned) arv_option_frame_retention * 1000,
			NULL);
	}

	gint payload = arv_camera_get_payload(handle->camera);
	for (int i = 0; i < nframes; i++)
		arv_stream_push_buffer(stream, arv_buffer_new(
			payload, &frame_buffer[i * payload]));

	arv_camera_set_acquisition_mode(handle->camera, ARV_ACQUISITION_MODE_CONTINUOUS);

	arv_camera_start_acquisition(handle->camera);

	gulong id1 = g_signal_connect(stream, "new-buffer",
		G_CALLBACK(on_capture_frame_impl), camera);
	if (!id1) return -1;
	arv_stream_set_emit_signals(stream, TRUE);

	gulong id2 = g_signal_connect(
		arv_camera_get_device(handle->camera), "control-lost",
		G_CALLBACK(on_camera_disconnected_impl), camera);
	if (!id2) return -1;
	
	handle->stream = stream;
	handle->on_capture_frame = on_capture_frame;
	handle->on_camera_disconnected = on_camera_disconnected;
	handle->user = user;
	
	return 0;
}

void genicam_loop_create(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	handle->loop = g_main_loop_new(NULL, FALSE);
	handle->context = g_main_loop_get_context(handle->loop);
}

void genicam_loop_iterate(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	g_main_context_iteration(handle->context, FALSE);
}

void genicam_loop_quit(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	if (handle->loop)
	{
		g_main_loop_quit(handle->loop);
		g_main_loop_unref(handle->loop);
		handle->loop = NULL;
	}
	if (handle->context)
	{
		g_main_context_unref(handle->context);
		handle->context = NULL;
	}
}

void genicam_stop(void* camera)
{
	genicam_t* handle = (genicam_t*)camera;
	arv_camera_stop_acquisition(handle->camera);
	if (handle->stream)
	{
		arv_stream_set_emit_signals(handle->stream, FALSE);
		g_object_unref(handle->stream);
		handle->stream = NULL;
	}
}

