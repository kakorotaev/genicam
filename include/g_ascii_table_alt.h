#ifndef G_ASCII_TABLE_ALT_H
#define G_ASCII_TABLE_ALT_H

// Workaround "undefined reference to `_imp__g_ascii_table'"
#ifdef __MINGW32__

#include <glib.h>

extern const guint16* const g_ascii_table_alt;

#define g_ascii_isspace_alt(c) \
	((g_ascii_table_alt[(guchar) (c)] & G_ASCII_SPACE) != 0)

#define g_ascii_isdigit_alt(c) \
        ((g_ascii_table_alt[(guchar) (c)] & G_ASCII_DIGIT) != 0)

#define g_ascii_isalpha_alt(c) \
        ((g_ascii_table_alt[(guchar) (c)] & G_ASCII_ALPHA) != 0)

#define g_ascii_isalnum_alt(c) \
        ((g_ascii_table_alt[(guchar) (c)] & G_ASCII_ALNUM) != 0)

#endif

#endif // G_ASCII_TABLE_ALT_H

